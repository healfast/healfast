SELECT *
FROM
(
    SELECT [Hopsital],
           [ID],
           [Tel],
           [DocName],
           [Submitted]
    FROM [dbo].[HealthcareRankings]
) AS SourceTable PIVOT(AVG([Submitted]) FOR [DocName] IN([Doc 1],
                                                         [Doc 2],
                                                         [Doc 3],
                                                         [Doc 4],
                                                         [Doc 5])) AS PivotTable;
